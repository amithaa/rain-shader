using UnityEngine;

public class RippleScript : MonoBehaviour
{
    private Material material;

    private float percentage;
    public float duration = 1;

    public bool isFired = false;
    //public Vector3 focalPoint;

    void Start()
    {
        if (TryGetComponent(out MeshRenderer renderer)) {
            material = renderer.material;
        }
    }

    void Update()
    {
        if (isFired) {
            percentage += Time.deltaTime / duration;
            material.SetFloat("_Percent", percentage);

            if (percentage > 1) {
                percentage = 0;
                isFired = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            percentage = 0;
            isFired = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        percentage = 0;
        //focalPoint = new Vector3(collision.transform.position.x, collision.transform.position.z, 0);
        //material.SetVector("_FocalPoint", focalPoint);
        isFired = true;
    }
}
